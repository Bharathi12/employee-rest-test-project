DROP TABLE IF EXISTS EMPLOYEE_TBL;
DROP TABLE IF EXISTS Department_TBL;

CREATE TABLE Department_TBL (
  department_id INT AUTO_INCREMENT  PRIMARY KEY,
  dept_name VARCHAR(250) NOT NULL
);

CREATE TABLE EMPLOYEE_TBL (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  first_name VARCHAR(250) NOT NULL,
  last_name VARCHAR(250) NOT NULL,
  salary DOUBLE DEFAULT NULL,
  department_id INT NOT NULL,
  PRIMARY KEY (id),
   CONSTRAINT department_id_fk FOREIGN KEY (department_Id) REFERENCES Department_TBL (department_Id) ON UPDATE CASCADE ON DELETE CASCADE
);


