package com.ibm.assign.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ibm.assign.entity.DepartmentEntity;
import com.ibm.assign.entity.EmpDeptPojo;


@Repository
public interface DepartmentRepository extends JpaRepository<DepartmentEntity, Long> {
	
	List<DepartmentEntity> findById(long id);
	

	@Query("select emp.empId as  employeeId, emp.fName as firstName, emp.lName as lastName, emp.salary as sal,   dept.deptName as deptName   from EmployeeEntity emp join DepartmentEntity dept on dept.id = emp.departmentId where dept.id=:id ")
	List<EmpDeptPojo> getEmpByDeptId(@Param("id") long id);
	
	
	

}
