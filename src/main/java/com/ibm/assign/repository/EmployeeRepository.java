package com.ibm.assign.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.ibm.assign.entity.EmployeeEntity;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Long> {

	List<EmployeeEntity> findByfName(String fName);
	
	EmployeeEntity findByEmpId(long id);

//	@Query("from EMPLOYEE_TBL where fname=?1 order by id")
//	List<EmployeeEntity>  findByNameSort(String fname);
	

}
