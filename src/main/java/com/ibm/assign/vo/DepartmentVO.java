package com.ibm.assign.vo;

import java.util.List;

public class DepartmentVO {
	
	private Long  id;
	private String deptName;
	private List EmployeeVO;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public List getEmployeeVO() {
		return EmployeeVO;
	}
	public void setEmployeeVO(List employeeVO) {
		EmployeeVO = employeeVO;
	}
	
	
	

}
