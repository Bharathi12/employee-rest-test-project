package com.ibm.assign.vo;

public class SalaryVO {
	
	private double fromSalary;
	private double toSalary;
	public double getFromSalary() {
		return fromSalary;
	}
	public void setFromSalary(double fromSalary) {
		this.fromSalary = fromSalary;
	}
	public double getToSalary() {
		return toSalary;
	}
	public void setToSalary(double toSalary) {
		this.toSalary = toSalary;
	}
	
	
	
	
}