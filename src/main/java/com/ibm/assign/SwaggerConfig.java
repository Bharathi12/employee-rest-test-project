package com.ibm.assign;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
//	/*
//	 * @Bean public Docket postsApi() { return new
//	 * Docket(DocumentationType.SWAGGER_2).groupName("public-api")
//	 * .apiInfo(apiInfo()).select().paths(postPaths()).build(); }
//	 * 
//	 * private Predicate<String> postPaths() { return or(regex("/api/posts.*"),
//	 * regex("/api/ibm.*")); }
//	 */
	
//	 @Bean
//	    public Docket api() { 
//	        return new Docket(DocumentationType.SWAGGER_2)  
//	          .select()                                  
//	          .apis(RequestHandlerSelectors.any())              
//	          .paths(PathSelectors.any())                          
//	          .build();                                           
//	    }
	
	@Bean
	   public Docket productApi() {
	      return new Docket(DocumentationType.SWAGGER_2).select()
	         .apis(RequestHandlerSelectors.basePackage("com.ibm.assign")).build();
	   }

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Employee Rest project API")
				.description("Employee Rest project API reference")				
				//.contact("javainuse@gmail.com").license("JavaInUse License")
				//.licenseUrl("javainuse@gmail.com")
				.version("1.0").build();
	}
	
	

}
