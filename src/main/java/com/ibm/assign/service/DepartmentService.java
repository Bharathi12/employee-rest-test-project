package com.ibm.assign.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibm.assign.entity.DepartmentEntity;
import com.ibm.assign.entity.EmpDeptPojo;
import com.ibm.assign.exception.RecordNotFoundException;
import com.ibm.assign.repository.DepartmentRepository;
import com.ibm.assign.vo.DepartmentVO;
import com.ibm.assign.vo.EmployeeVO;

@Service
public class DepartmentService {
	
	@Autowired
	DepartmentRepository  deptrepository;
	
	
	private DepartmentVO convertToDeptVO(DepartmentEntity dept) {
		DepartmentVO deptVO = new DepartmentVO();
		deptVO.setId(dept.getId());
		deptVO.setDeptName(dept.getDeptName());
		//deptVO.setEmployeeVO(employeeVO);===create new method to conver to empVO
		return deptVO;
	}
	
	public DepartmentVO createDept(DepartmentVO entity) {
		DepartmentEntity deptEntity = new DepartmentEntity();
		deptEntity.setId(entity.getId());
		deptEntity.setDeptName(entity.getDeptName());		
		deptrepository.save(deptEntity);			
		return entity;
	}
	
	public DepartmentVO updateDept(DepartmentVO entity) {
		DepartmentEntity deptEntity = new DepartmentEntity();
		deptEntity.setId(entity.getId());
		deptEntity.setDeptName(entity.getDeptName());
		deptrepository.save(deptEntity);			
		return entity;
	}
	
	public List<DepartmentVO> getDept() {
		List<DepartmentEntity> deptList = deptrepository.findAll();
		List<DepartmentVO> deptVO = deptList.stream().map(dept -> convertToDeptVO(dept)).collect(Collectors.toList());
		
			return deptVO;
		
	}
	
	public boolean deleteDept(Long id) throws RecordNotFoundException {		
		
		Optional<DepartmentEntity> deptlist = deptrepository.findById(id);
		boolean isDeleted = false;
		if (deptlist.isPresent()) {
			deptrepository.deleteById(id);
			isDeleted = true;
		} else {
			throw new RecordNotFoundException("No dept exist for given id");
		}
		return isDeleted;
	}
	
	public DepartmentVO getEmpByDeptId(Long id) {
		
		List<EmpDeptPojo> empDeptList = deptrepository.getEmpByDeptId(id);		
		DepartmentVO deptVO = convertToDeptVO(empDeptList.get(0),id);
		List<EmployeeVO> empVO = empDeptList.stream().map(dept -> convertToEmpVO(dept)).collect(Collectors.toList());
		deptVO.setEmployeeVO(empVO);
		return deptVO;
		
	}
	
	private EmployeeVO convertToEmpVO(EmpDeptPojo dept) {		
		EmployeeVO empVO = new EmployeeVO();
		//empVO.setId(dept.ge());
		empVO.setfName(dept.getFirstName());
		empVO.setlName(dept.getLastName());
		empVO.setSalary(dept.getSal());
		empVO.setEmpId(dept.getEmployeeId());		
		return empVO;
		
	}
	
	private DepartmentVO convertToDeptVO(EmpDeptPojo dept, Long id) {
		DepartmentVO deptVO = new DepartmentVO();		
		deptVO.setId(id);
		deptVO.setDeptName(dept.getDeptName());	
		

		return deptVO;
	}

}
