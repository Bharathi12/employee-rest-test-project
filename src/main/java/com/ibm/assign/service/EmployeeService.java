package com.ibm.assign.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.ibm.assign.entity.EmployeeEntity;
import com.ibm.assign.exception.RecordNotFoundException;
import com.ibm.assign.repository.EmployeeRepository;
import com.ibm.assign.vo.EmployeeVO;

@Service
public class EmployeeService {

	@Autowired
	EmployeeRepository repository;

	private final Logger log = LoggerFactory.getLogger(EmployeeService.class);

	public boolean createEmployee(EmployeeVO entity) throws Exception {

		String METHOD_NAME = "createEmployee";
		log.info("Inside method" + METHOD_NAME);

		if (log.isDebugEnabled()) {

			log.debug("*******" + entity.toString());
		}

		boolean isSaved = false;
		try {

			EmployeeEntity empEntity = new EmployeeEntity();
			empEntity.setEmpId(entity.getEmpId());
			empEntity.setfName(entity.getfName());
			empEntity.setlName(entity.getlName());
			empEntity.setSalary(entity.getSalary());
			empEntity.setDepartment_Id(entity.getDepartmentId());
			repository.save(empEntity);
			isSaved = true;

		} catch (Exception e) {
			log.error("Error occurred while Saving emoloyeee details******");
		}
		return isSaved;
	}

	public EmployeeVO updateEmployee(EmployeeVO entity) throws Exception {

		String METHOD_NAME = "updateEmployee";
		log.info("Inside method" + METHOD_NAME);

		if (log.isDebugEnabled()) {

			log.debug("*******" + entity.toString());
		}

		try {

			EmployeeEntity empEntity = new EmployeeEntity();
			empEntity.setEmpId(entity.getEmpId());
			empEntity.setfName(entity.getfName());
			empEntity.setlName(entity.getlName());
			empEntity.setSalary(entity.getSalary());
			empEntity.setDepartment_Id(entity.getDepartmentId());
			repository.save(empEntity);

		} catch (Exception e) {
			log.error("Error occurred while Saving emoloyeee details******");

		}
		return entity;
	}

	public List<EmployeeVO> findByFname(String fName) throws RecordNotFoundException {

		String METHOD_NAME = "findByFame";
		log.info("Inside method" + METHOD_NAME);

		if (log.isDebugEnabled()) {

			log.debug("*******" + fName.toString());
		}

		List<EmployeeEntity> empEntity = repository.findByfName(fName);
		List<EmployeeVO> empList = empEntity.stream().map(emp -> convertToEmpVO(emp)).collect(Collectors.toList());

		return empList;

	}

	private EmployeeVO convertToEmpVO(EmployeeEntity emp) {
		EmployeeVO empVO = new EmployeeVO();
		empVO.setEmpId(emp.getEmpId());
		empVO.setfName(emp.getfName());
		empVO.setlName(emp.getlName());
		empVO.setSalary(emp.getSalary());
		empVO.setDepartmentId(emp.getDepartmentId());

		return empVO;
	}

	public EmployeeVO findById(long id) throws RecordNotFoundException {

		String METHOD_NAME = "findById";
		log.info("Inside method" + METHOD_NAME);

		if (log.isDebugEnabled()) {

			log.debug("*******" + id);
		}

		EmployeeEntity emp = repository.findByEmpId(id);
		EmployeeVO empVO = convertToEmpVO(emp);
		return empVO;

	}

	public void deleteEmployeeById(Long id) throws RecordNotFoundException {
		Optional<EmployeeEntity> employee = repository.findById(id);

		String METHOD_NAME = "deleteEmployeeById";
		log.info("Inside method" + METHOD_NAME);

		if (log.isDebugEnabled()) {

			log.debug("*******" + id);
		}

		try {

			if (employee.isPresent()) {
				repository.deleteById(id);
			} else {

				throw new RecordNotFoundException("No employee record exist for given id");

			}
		}

		catch (Exception e) {
			log.error("No employee record exist for given id");
		}

	}

	public List<EmployeeVO> getEmployees() {

		List<EmployeeEntity> employeeList = repository.findAll();
		List<EmployeeVO> empList = employeeList.stream().map(emp -> convertToEmpVO(emp)).collect(Collectors.toList());

		return empList;

	}

	public List<EmployeeVO> sortByFname() {

		Sort sortOrder = Sort.by("fName");
		List<EmployeeEntity> employeeList = repository.findAll(sortOrder);
		List<EmployeeVO> empVO = employeeList.stream().map(emp -> convertToEmpVO(emp)).collect(Collectors.toList());
		return empVO;

	}

	public List<EmployeeVO> sortById() {

		Sort sortOrder = Sort.by("id");
		List<EmployeeEntity> employeeList = repository.findAll(sortOrder);
		List<EmployeeVO> empVO = employeeList.stream().map(emp -> convertToEmpVO(emp)).collect(Collectors.toList());
		return empVO;
	}

	public EmployeeVO filterByEmployeeSal(Double fromSalary, Double toSalary) {
		return new EmployeeVO();
	}

}
