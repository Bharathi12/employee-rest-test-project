package com.ibm.assign.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ibm.assign.exception.RecordNotFoundException;
import com.ibm.assign.service.DepartmentService;
import com.ibm.assign.vo.DepartmentVO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class DepartmentController {

	@Autowired
	DepartmentService deptservice;

	@ApiOperation("Create Department")
	@ApiResponses(value = { @ApiResponse(code = 207, message = "Multiple responses available"),
			@ApiResponse(code = 400, message = "Request contains invalid parameters") })
	@PostMapping("/create-dept")
	public ResponseEntity<DepartmentVO> createDept(@RequestBody DepartmentVO entity) throws Exception {
		DepartmentVO list = deptservice.createDept(entity);
		return new ResponseEntity<DepartmentVO>(list, new HttpHeaders(), HttpStatus.OK);
	}

	@ApiOperation("Update Employee")
	@ApiResponses(value = { @ApiResponse(code = 207, message = "Multiple responses available"),
			@ApiResponse(code = 400, message = "Request contains invalid parameters") })
	@PutMapping("/update-dept")
	public ResponseEntity<DepartmentVO> updateDept(@RequestBody DepartmentVO entity) throws Exception {
		DepartmentVO list = deptservice.updateDept(entity);

		return new ResponseEntity<DepartmentVO>(list, new HttpHeaders(), HttpStatus.OK);
	}

	@ApiOperation("Get list of Departments")
	@ApiResponses(value = { @ApiResponse(code = 207, message = "Multiple responses available"),
			@ApiResponse(code = 400, message = "Request contains invalid parameters") })
	@GetMapping("/dept")
	public ResponseEntity<List<DepartmentVO>> getDept() throws Exception {
		List<DepartmentVO> list = deptservice.getDept();

		return new ResponseEntity<List<DepartmentVO>>(list, new HttpHeaders(), HttpStatus.OK);
	}

	@ApiOperation("Delete Department")
	@ApiResponses(value = { @ApiResponse(code = 207, message = "Multiple responses available"),
			@ApiResponse(code = 400, message = "Request contains invalid parameters") })
	@DeleteMapping("/delete-dept/id")
	public ResponseEntity<?> deleteDept(@RequestParam("id") Long id) throws RecordNotFoundException {

		boolean isDeleted = deptservice.deleteDept(id);
		return new ResponseEntity<>(isDeleted, HttpStatus.OK);

	}

	@ApiOperation("List of employees by Dept")
	@ApiResponses(value = { @ApiResponse(code = 207, message = "Multiple responses available"),
			@ApiResponse(code = 400, message = "Request contains invalid parameters") })
	@GetMapping("/emp-by-dept/id")
	public ResponseEntity<DepartmentVO> getEmpByDeptId(@RequestParam("id") Long id) throws Exception {

		return new ResponseEntity<>(deptservice.getEmpByDeptId(id), HttpStatus.OK);

	}

}
