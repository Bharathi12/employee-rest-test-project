package com.ibm.assign.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ibm.assign.exception.RecordNotFoundException;
import com.ibm.assign.service.EmployeeService;
import com.ibm.assign.vo.EmployeeVO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class EmployeeController {
	
	private final Logger log = LoggerFactory.getLogger(EmployeeController.class);

	@Autowired
	EmployeeService service;	
	
	@ApiOperation("Create Employee")
	@ApiResponses(value = {
	@ApiResponse(code = 207, message = "Multiple responses available"),
    @ApiResponse(code = 400, message = "Request contains invalid parameters")})
	@PostMapping("/create-employee")
	public ResponseEntity<?> createEmployee(@RequestBody EmployeeVO entity)  throws Exception{
		boolean isSaved = service.createEmployee(entity);
		return new ResponseEntity<>(isSaved, HttpStatus.OK);
	}
	
	@ApiOperation("Update Employee")
	@ApiResponses(value = {
	@ApiResponse(code = 207, message = "Multiple responses available"),
    @ApiResponse(code = 400, message = "Request contains invalid parameters")})
	@PutMapping("/update-employee")
	public ResponseEntity<EmployeeVO> UpdateEmployee(@RequestBody EmployeeVO entity) throws Exception {
		EmployeeVO list = service.updateEmployee(entity);

		return new ResponseEntity<EmployeeVO>(list, HttpStatus.OK);
	}

	@ApiOperation("Find by Name")
	@ApiResponses(value = {
	@ApiResponse(code = 207, message = "Multiple responses available"),
    @ApiResponse(code = 400, message = "Request contains invalid parameters")})
	@GetMapping("/find-by-name/fName/{fName}")
	public List<EmployeeVO> findByName(@PathVariable("fName") String fName) throws RecordNotFoundException {

		List<EmployeeVO> list = service.findByFname(fName);

		return list;
	}
	
	@ApiOperation("Find by Id")
	@ApiResponses(value = {
	@ApiResponse(code = 207, message = "Multiple responses available"),
    @ApiResponse(code = 400, message = "Request contains invalid parameters")})
	@GetMapping("/find-by-id/id/{id}")
	public EmployeeVO findById(@PathVariable("id") long id) throws RecordNotFoundException {

		EmployeeVO list = service.findById(id);

		return list;
	}
	
	@ApiOperation("Delete Employee")
	@ApiResponses(value = {
	@ApiResponse(code = 207, message = "Multiple responses available"),
    @ApiResponse(code = 400, message = "Request contains invalid parameters")})
	@DeleteMapping("/delete-employee/id")
	public ResponseEntity<?> deleteEmployeeById(@RequestParam("id") Long id) throws RecordNotFoundException {
		
		boolean isDeleted = false;
		
		service.deleteEmployeeById(id);
        
		isDeleted = true;
		return  new ResponseEntity<>( isDeleted, HttpStatus.OK);

	}
	
	@ApiOperation("Get list of Employee")
	@ApiResponses(value = {
	@ApiResponse(code = 207, message = "Multiple responses available")})
	@GetMapping("/employee")
	public ResponseEntity<List<EmployeeVO>> getEmployees() throws Exception {
		
		if(log.isDebugEnabled()) {
			
			log.debug("*******");
		}
		List<EmployeeVO> list = service.getEmployees();			
		
		return new ResponseEntity<List<EmployeeVO>>(list, new HttpHeaders(), HttpStatus.OK);
	}

	
	
	
	@ApiOperation("Sort by first name")
	@ApiResponses(value = {
	@ApiResponse(code = 207, message = "Multiple responses available"),
    @ApiResponse(code = 400, message = "Request contains invalid parameters")})
 	 @GetMapping("/employee/sort-by-fname")
 	    public ResponseEntity<?> sortByFname() throws Exception{
 	    	List<EmployeeVO> list = service.sortByFname();
 			
 			return new ResponseEntity<>(list, HttpStatus.OK);
 			
 	}

	
	@ApiOperation("Sort by employee Id")
	@ApiResponses(value = {
	@ApiResponse(code = 207, message = "Multiple responses available"),
    @ApiResponse(code = 400, message = "Request contains invalid parameters")})
     @GetMapping("/employee/sort-by-id")
    public ResponseEntity<?> sortById() throws Exception{
     	List<EmployeeVO> list = service.sortById();
 		
 		return new ResponseEntity<>(list, HttpStatus.OK);
 		
 	}
	
	
	@ApiOperation("Filter by Salary")
	@ApiResponses(value = {
	        @ApiResponse(code = 201, message = "Connector configuration for the given zone is successfully created.") })
	@PostMapping("/filter-by-sal")
	public ResponseEntity<List<EmployeeVO>> filterByEmployeeSal(@RequestParam("fromSalary") double fromSal, @RequestParam("toSalary") double toSal) throws Exception {
		List<EmployeeVO> list = service.getEmployees();			
		
		
		List<EmployeeVO> filteredList = list.stream().filter(employee -> employee.getSalary() >= fromSal && employee.getSalary() <= toSal).collect(Collectors.toList());

		
		return new ResponseEntity<List<EmployeeVO>>(filteredList, new HttpHeaders(), HttpStatus.OK);
	}

}
