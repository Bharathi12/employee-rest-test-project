package com.ibm.assign.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="EMPLOYEE_TBL")
public class EmployeeEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	@Column(name="id")
    private Long empId;
	
	@Column(name="first_name")
	private String fName;
	
	@Column(name="last_name")
	private String lName;
	
	@Column(name="Salary")
	private double salary;
	
	@Column(name="department_id")
	private int departmentId;
	
	
	@ManyToOne(optional = false)
    @JoinColumn(name="department_Id", insertable=false, updatable=false) //means this relationship becomes mandatory , no employee row can be saved without a dept id reference
                                 // emp table is not allowed to update without value in dept
	 private DepartmentEntity department;

	
	public Long getEmpId() {
		return empId;
	}

	public void setEmpId(Long empId) {
		this.empId = empId;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartment_Id(int department_Id) {
		this.departmentId = department_Id;
	}	
	

//	public DepartmentEntity getDepartment() {
//		return department;
//	}
//
//	public void setDepartment(DepartmentEntity department) {
//		this.department = department;
//	}

	@Override
	public String toString() {
		return "EmployeeEntity [id=" + empId + ", fName=" + fName + ", lName=" + lName + ", salary=" + salary
				+ ", department_Id=" + departmentId + "]";
	}

	
	
	
	
}
