package com.ibm.assign.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name="DEPARTMENT_TBL")
public class DepartmentEntity {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	@Column(name = "department_id")
    private Long  id;
 
    @Column(name = "dept_name")
    private String deptName;

    @OneToMany
    @JoinColumn(name="department_Id", insertable=false, updatable=false) //means this relationship becomes mandatory , no employee row can be saved without a dept id reference
                                 // emp table is not allowed to update without value in dept
	 private List<EmployeeEntity> empList;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	
	

	@Override
	public String toString() {
		return "DepartmentEntity [id=" + id + ", deptName=" + deptName + "]";
	}
	
    
	

}
