package com.ibm.assign.service;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibm.assign.entity.DepartmentEntity;
import com.ibm.assign.entity.EmpDeptPojo;
import com.ibm.assign.entity.EmployeeEntity;
import com.ibm.assign.exception.RecordNotFoundException;
import com.ibm.assign.repository.DepartmentRepository;
import com.ibm.assign.vo.DepartmentVO;
import com.ibm.assign.vo.EmployeeVO;

@RunWith(MockitoJUnitRunner.class)
public class DepartmentServiceTest {
	
	@InjectMocks
	 DepartmentService deptservice;
	
	@Mock
	DepartmentRepository deptrepository;
	
	MockMvc mvc;
	
	ObjectMapper mapper = new ObjectMapper();

	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.standaloneSetup(deptservice).build();
	}

	@Test
	public void testCreateDept() throws Exception{
		DepartmentVO deptVo = new DepartmentVO();
		deptVo.setId(1L);
		deptVo.setDeptName("Accounts");
		DepartmentEntity deptEntity =  new DepartmentEntity();
		when(deptrepository.save(ArgumentMatchers.any())).thenReturn(deptEntity);
		DepartmentVO result =deptservice.createDept(deptVo);
		assertEquals(deptVo, result);
	}

	@Test
	public void testUpdateDept() throws Exception {
		DepartmentVO entit = new DepartmentVO();
		entit.setId(1L);
		entit.setDeptName("Accounts updated");
		DepartmentEntity deptEntity =  new DepartmentEntity();
		when(deptrepository.save(ArgumentMatchers.any())).thenReturn(deptEntity);
		DepartmentVO result =deptservice.updateDept(entit);
		assertEquals(entit.getDeptName(), result.getDeptName());
	}

	@Test
	public void testGetDept() throws Exception{	
		List<DepartmentEntity> deptEntity =  new ArrayList<>();
		DepartmentEntity entity = new DepartmentEntity();
		entity.setDeptName("Sales");
		deptEntity.add(entity);
		when(deptrepository.findAll()).thenReturn(deptEntity);
		assertEquals(entity.getDeptName(), deptservice.getDept().get(0).getDeptName());
	}

//	@Test
//	public void testDeleteDept() throws RecordNotFoundException {
//		DepartmentVO entit = new DepartmentVO();
//		entit.setId(1L);
//		when(deptrepository.save(ArgumentMatchers.any())).thenReturn(entit);
////		doNothing().when(deptrepository.deleteById(ArgumentMatchers.anyLong()));
////		doThrow(new RuntimeException("Testingggg")).when(deptrepository.deleteById(ArgumentMatchers.any()));
//		 //boolean result =deptservice.deleteDept(1L);
//		 assertEquals(deptservice.deleteDept(1L), "No Record found");
//		
//	}
//	
//	
//	
//
//	@Test
//	public void testGetEmpByDeptId() throws Exception {
//		DepartmentVO dept = new DepartmentVO();
//		dept.setId(1L);
//		dept.setDeptName("Accounts");		
////		when(deptrepository.getEmpByDeptId(ArgumentMatchers.anyLong())).thenReturn(dept);		
//		DepartmentVO deptVO = deptservice.getEmpByDeptId(1L);
//		assertEquals(deptVO.getDeptName(), "Accounts");
//	}

}
