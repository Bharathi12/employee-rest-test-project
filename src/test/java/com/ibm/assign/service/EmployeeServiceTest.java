package com.ibm.assign.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Sort;

import com.ibm.assign.entity.EmployeeEntity;
import com.ibm.assign.exception.RecordNotFoundException;
import com.ibm.assign.repository.EmployeeRepository;
import com.ibm.assign.vo.EmployeeVO;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {
	
	
	@InjectMocks
	 EmployeeService service;
	
	@Mock
	EmployeeRepository repository;
	

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testCreateEmployee() throws Exception{
		EmployeeVO entit = new EmployeeVO();
		entit.setDepartmentId(10);
		entit.setEmpId(1L);
		entit.setfName("Niyati");
		entit.setlName("k");
		entit.setSalary(3333.00);
		EmployeeEntity empEntity =  new EmployeeEntity();
		when(repository.save(ArgumentMatchers.any())).thenReturn(empEntity);
		boolean result =service.createEmployee(entit);
		

	}

	@Test
	public void testUpdateEmployee() throws Exception {
		EmployeeVO entit = new EmployeeVO();
		entit.setDepartmentId(10);
		entit.setEmpId(1L);
		entit.setfName("Niyati");
		entit.setlName("k");
		entit.setSalary(3333.00);
		EmployeeEntity empEntity =  new EmployeeEntity();
		when(repository.save(ArgumentMatchers.any())).thenReturn(empEntity);
		assertEquals(entit.getfName(), service.updateEmployee(entit).getfName());
	}
	
	
	
//	@Test(expected=Exception.class)
//	public void testCreateEmployee_Exception() throws Exception {
//		EmployeeVO entit = new EmployeeVO();
//		entit.setDepartmentId(10);
//		entit.setEmpId(1L);
//		entit.setfName("Niyati");
//		entit.setlName("k");
//		entit.setSalary(3333.00);
//		EmployeeEntity empEntity =  new EmployeeEntity();
//		when(repository.save(ArgumentMatchers.any())).thenThrow(new RuntimeException("Testingggg"));
//		boolean result =service.createEmployee(entit);
//	}

	@Test
	public void testFindByfName() throws RecordNotFoundException {
		List<EmployeeEntity> entities = new ArrayList<EmployeeEntity>();
		EmployeeEntity emp = new EmployeeEntity();
		emp.setDepartment_Id(10);
		emp.setEmpId(1L);
		emp.setfName("Niyati");
		emp.setlName("k");
		emp.setSalary(3333.00);
		entities.add(emp);
		when( repository.findByfName(ArgumentMatchers.any())).thenReturn(entities);
		
		List<EmployeeVO> empl = service.findByFname("Niyati");
		assertEquals(emp.getSalary(), empl.get(0).getSalary());
	}

	@Test
	public void testFindByid()  throws RecordNotFoundException {
		EmployeeEntity emp = new EmployeeEntity();
		emp.setDepartment_Id(10);
		emp.setEmpId(1L);
		emp.setfName("Niyati");
		emp.setlName("k");
		emp.setSalary(3333.00);
		when( repository.findByEmpId(ArgumentMatchers.anyLong())).thenReturn(emp);
		
		EmployeeVO empl = service.findById(0);
		assertEquals(empl.getSalary(), 3333.00);
		
	}

	@Test
	public void testDeleteEmployeeById() {
		//fail("Not yet implemented");
	}

	@Test
	public void testGetEmployees() {
		List<EmployeeEntity> entities = new ArrayList<EmployeeEntity>();
		EmployeeEntity emp = new EmployeeEntity();
		emp.setDepartment_Id(10);
		emp.setEmpId(1L);
		emp.setfName("Niyati");
		emp.setlName("k");
		emp.setSalary(3333.00);
		entities.add(emp);
		when( repository.findAll()).thenReturn(entities);
		assertEquals(emp.getDepartmentId(), service.getEmployees().get(0).getDepartmentId());
	}

//	@Test
//	public void testSortByFname() {
//		List<EmployeeEntity> entities = new ArrayList<EmployeeEntity>();
//		EmployeeEntity emp = new EmployeeEntity();
//		emp.setDepartment_Id(10);
//		emp.setEmpId(1L);
//		emp.setfName("Niyati");
//		emp.setlName("k");
//		emp.setSalary(3333.00);
//		entities.add(emp);
//		EmployeeEntity emp1 = new EmployeeEntity();
//		emp1.setfName("Hayati");
//		entities.add(emp1);
//		when(repository.findAll(Sort.by("fName"))).thenReturn(entities);
//		assertEquals(emp1.getfName(), service.sortByFname().get(0).getfName());
//	}

//	@Test
//	public void testSortById() {
//		List<EmployeeEntity> entities = new ArrayList<EmployeeEntity>();
//		EmployeeEntity emp = new EmployeeEntity();
//		emp.setDepartment_Id(10);
//		emp.setEmpId(2L);
//		emp.setfName("Niyati");
//		emp.setlName("k");
//		emp.setSalary(3333.00);
//		entities.add(emp);
//		EmployeeEntity emp1 = new EmployeeEntity();
//		emp1.setEmpId(1L);
//		entities.add(emp1);
//		when(repository.findAll(Sort.by("id"))).thenReturn(entities);		
//		assertEquals(emp1.getEmpId(), service.sortById().get(0).getEmpId());
//	}

	@Test
	public void testFilterByEmployeeSal() {
		//fail("Not yet implemented");
	}

}
