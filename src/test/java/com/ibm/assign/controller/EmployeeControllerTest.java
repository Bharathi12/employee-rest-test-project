package com.ibm.assign.controller;

import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibm.assign.service.EmployeeService;
import com.ibm.assign.vo.EmployeeVO;

@RunWith(SpringRunner.class)
public class EmployeeControllerTest {
	
	@Mock
	private EmployeeService empService;
	
	@InjectMocks
	EmployeeController empContoller;
	
	
	MockMvc mvc;
	
	ObjectMapper mapper = new ObjectMapper();


	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.standaloneSetup(empContoller).build();
	}

	@Test
	public void testCreateEmployee() throws Exception {
		EmployeeVO entit = new EmployeeVO();
		entit.setDepartmentId(10);
		entit.setEmpId(1L);
		entit.setfName("Niyati");
		entit.setlName("k");
		entit.setSalary(3333.00);
		
		when(empService.createEmployee(entit)).thenReturn(true);
		
		mvc.perform(post("/create-employee")
				.content(mapper.writeValueAsString(entit))
	            .contentType(MediaType.APPLICATION_JSON))
	            .andExpect(status().isOk())
	            .andReturn();
		
	}

	@Test
	public void testUpdateEmployee() throws Exception {
		EmployeeVO entit = new EmployeeVO();
		entit.setDepartmentId(10);
		entit.setEmpId(1L);
		entit.setfName("Niyati");
		entit.setlName("k");
		entit.setSalary(3333.00);
		
		mvc.perform(put("/update-employee")
				.content(mapper.writeValueAsString(entit))
	            .contentType(MediaType.APPLICATION_JSON))
	            .andExpect(status().isOk())
	            .andReturn();
	}

	@Test
	public void testFindByName() throws Exception {
		EmployeeVO entit = new EmployeeVO();
		entit.setDepartmentId(10);
		entit.setEmpId(1L);
		entit.setfName("Niyati");
		entit.setlName("k");
		entit.setSalary(3333.00);
		
		mvc.perform(get("/find-by-name/fName/Niyati"))
	            .andExpect(status().isOk())
	            .andReturn();
	}

	@Test
	public void testFindByid() throws Exception {
		
		
		mvc.perform(get("/find-by-id/id/1"))
        .andExpect(status().isOk())
        .andReturn();
	
	}
		

	@Test
	public void testDeleteEmployeeById() throws Exception {
		mvc.perform(delete("/delete-employee/id?id=1"))
        .andExpect(status().isOk())
        .andReturn();
	}

	@Test
	public void testGetEmployees() throws Exception {
		mvc.perform(get("/employee"))
        .andExpect(status().isOk())
        .andReturn();
	}

	@Test
	public void testSortByFname() throws Exception {
		mvc.perform(get("/employee/sort-by-fname"))
        .andExpect(status().isOk())
        .andReturn();
	}

	@Test
	public void testSortById()  throws Exception {

		mvc.perform(get("/employee/sort-by-id")
	            .contentType(MediaType.APPLICATION_JSON))
	            .andExpect(status().isOk())
	            .andReturn();
	
	}

	@Test
	public void testFilterByEmployeeSal() throws Exception {
		
	}

}
