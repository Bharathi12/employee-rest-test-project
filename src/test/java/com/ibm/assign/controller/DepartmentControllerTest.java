package com.ibm.assign.controller;

import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibm.assign.service.DepartmentService;
import com.ibm.assign.vo.DepartmentVO;

@RunWith(SpringRunner.class)
public class DepartmentControllerTest {
	
	@Mock
	private DepartmentService deptservice;
	
	@InjectMocks
	DepartmentController deptcontoller;
	
	
	MockMvc mvc;
	
	ObjectMapper mapper = new ObjectMapper();

	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.standaloneSetup(deptcontoller).build();
		
		
	}

	@Test
	public void testCreateDept() throws Exception{
		
		DepartmentVO departmentVO = new DepartmentVO();
		departmentVO.setId(1L);
		departmentVO.setDeptName("Accounts");
		
		mvc.perform(post("/create-dept")
				.content(mapper.writeValueAsString(departmentVO))
	            .contentType(MediaType.APPLICATION_JSON))
	            .andExpect(status().isOk())
	            .andReturn();
		
		
	}

	@Test
	public void testUpdateDept() throws Exception{
		DepartmentVO departmentVO = new DepartmentVO();
		departmentVO.setId(1L);
		departmentVO.setDeptName("Accounts");
		
		mvc.perform(put("/update-dept")
				.content(mapper.writeValueAsString(departmentVO))
	            .contentType(MediaType.APPLICATION_JSON))
	            .andExpect(status().isOk())
	            .andReturn();
	}

	@Test
	public void testGetDept() throws Exception{
		
		mvc.perform(get("/dept")				
	            .contentType(MediaType.APPLICATION_JSON))
	            .andExpect(status().isOk())
	            .andReturn();
	}

	@Test
	public void testDeleteDept() throws Exception{		
		
		mvc.perform(delete("/delete-dept/id?id=1"))
	            .andExpect(status().isOk());
	            
		
	}

	@Test
	public void testGetEmpByDeptId() throws Exception{		
		
		
		mvc.perform(get("/emp-by-dept/id?id=1")
	            .contentType(MediaType.APPLICATION_JSON))
	            .andExpect(status().isOk())
	            .andReturn();
	}

}
